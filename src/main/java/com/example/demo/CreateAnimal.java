package com.example.demo;

import lombok.Value;

/**
 * @author pilzhere
 * @created 03/01/2022 - 9:51 PM
 * @project demo
 */

@Value
public class CreateAnimal {
    String name;
    String binomialName;
}
