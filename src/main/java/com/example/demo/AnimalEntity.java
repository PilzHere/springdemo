package com.example.demo;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author pilzhere
 * @created 03/01/2022 - 11:05 PM
 * @project demo
 */

@Data
@AllArgsConstructor
public class AnimalEntity {
    String id;
    String name;
    String binomialName;
    String description;
    String conservationStatus;
}
