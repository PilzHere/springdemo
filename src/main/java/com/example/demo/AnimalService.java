package com.example.demo;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

/**
 * @author pilzhere
 * @created 03/01/2022 - 10:59 PM
 * @project demo
 */

@Service
public class AnimalService {

    public Stream<AnimalEntity> all () {
        return Stream.of(
                new AnimalEntity(UUID.randomUUID().toString(), "Donkey", "X", "", ""),
                new AnimalEntity(UUID.randomUUID().toString(), "Penguin", "Y", "", ""),
                new AnimalEntity(UUID.randomUUID().toString(), "Monkey", "Z", "", ""),
                new AnimalEntity(UUID.randomUUID().toString(), "Turtle", "W", "", "")
        );
    }

    public AnimalEntity createAnimal (String name, String binomialName) {
        return new AnimalEntity(
                UUID.randomUUID().toString(),
                name,
                binomialName,
                "",
                "");
    }

    public AnimalEntity get (String id) {
        return new AnimalEntity(
                id,
                "Goose",
                "G",
                "",
                "");
    }

    public AnimalEntity updateAnimal (String id, String name, String binomialName) {
        return new AnimalEntity(
                id,
                name,
                binomialName,
                "",
                "");
    }

    public void delete (String id) {
        // empty
    }
}
