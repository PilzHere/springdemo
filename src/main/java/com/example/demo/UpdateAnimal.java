package com.example.demo;

import lombok.Value;

/**
 * @author pilzhere
 * @created 03/01/2022 - 10:46 PM
 * @project demo
 */

@Value
public class UpdateAnimal {
    String name;
    String binomialName;
}
