package com.example.demo;

import lombok.Value;

import java.util.UUID;

/**
 * @author pilzhere
 * @created 03/01/2022 - 1:53 PM
 * @project demo
 */

@Value
public class Animal {
    String id;
    String name;
    String description;
    String conservationStatus;
    String binomialName;

    public Animal (String id, String name, String binomialName, String description, String conservationStatus) {
        this.name = name;
        this.binomialName = binomialName;
        this.id = id;

        this.description = null;
        this.conservationStatus = null;
    }
}
